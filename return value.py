#Fungsi dengan Return Value
def kuadrat(argumen):
    total = argumen**2
    print('nilai dari', argumen,'adalah', total)
    return total #untuk return total agar tidak none
#a = kuadrat(3) //Jika tidak d return maka hasilnya none
print(kuadrat(4))
print('+'*100)

#fungsi dengan return valur multiplr argumen
def tambah(argumen1,argumen2):
    total = argumen1+argumen2
    print(argumen1, '+',argumen2,'=',total)
    return total

def kali(argumen1,argumen2):
    total = argumen1*argumen2
    print(argumen1, 'x',argumen2,'=',total)
    return total
a = tambah(4,6)
b = kali(10,a)
print(b)

c = kali(3,tambah(7,10)) #Perkalian dan penjumlahan dalam satu fungsi
