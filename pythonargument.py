
# fungsi dengan menggunakan argumen sederhana
def siswa(nama):
    print('siswa ini bernama', nama)
# siswa('mario')

# fungsi dengan menggunakan keyword arguments
def guru(nama, mapel):
    print('guru ini bernama', nama)
    print('mengajar:', mapel)
#
# guru(nama='tamam',mapel='pemrograman')
#
# guru(mapel='olahraga',nama='pak sulis')

#  fungsi menggunakan default
def staff(nama, shift='pagi',galak='tidak'):
    print('guru ini bernama', nama)
    print('shifnya:', shift)
    print('sifat:', galak)
staff('riyo')
staff('maman',shift='malam')
staff('pak asep',galak='iya')
staff(shift='malam',galak='tidak') #error karena nama tidak default